const nodemailer = require('nodemailer');

const main = async () => {
    const transporter = nodemailer.createTransport({
        host: process.env.MTA_HOST,
        port: 25,
        secure: false,
        tls: {
            rejectUnauthorized: false
        }
    });

    const body = `This is the HTML message body <b>in bold!</b>`;

    const info = await transporter.sendMail({
        from: '"SUPAWIT WANNAPILA <no-reply-supawit-poc@cmu.ac.th"',
        replyTo: 'supawit.w@cmu.ac.th',
        to: process.env.RECIPIENT,
        subject: `${new Date()} -Here is the subject`,
        html: body
    });

    console.log("Message sent: %s",JSON.stringify(info));

}
main().catch(console.error);
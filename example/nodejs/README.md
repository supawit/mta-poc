1. install package
```
docker run --rm -it -w /app -v ${PWD}:/app node:alpine npm install
```
2. run script
```
MTA_HOST=<mta_host_or_ip>
RECIPIENT_EMAIL_ADDRESS=<recipient_email_address>
docker run --rm -it -w /app -v ${PWD}:/app -e MTA_HOST=$MTA_HOST -e RECIPIENT=$RECIPIENT_EMAIL_ADDRESS node:alpine node index
```

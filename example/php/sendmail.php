<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

require 'vendor/autoload.php';

$mail = new PHPMailer(true);

try {
    //Server settings
    $mail->isSMTP();
    $mail->Host         = getenv("MTA_HOST");
    $mail->SMTPAuth     = false;
    $mail->Port         = 25;
    $mail->SMTPSecure   = false;
    $mail->SMTPAutoTLS = false;

    //Recipients
    $mail->setFrom('no-reply-supawit-poc@cmu.ac.th');
    $mail->addReplyTo('supawit.w@cmu.ac.th');
    $mail->addAddress(getenv("RECIPIENT"));

    //Content
    $mail->isHTML(true);                                  
    $mail->Subject = time().'-Here is the subject';
    $mail->Body    = 'This is the HTML message body <b>in bold!</b>';

    $mail->send();
    echo 'Message has been sent';
} catch (\Throwable $th) {
    echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
}
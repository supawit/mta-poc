1. install PHPMailer via composer
```
docker run --rm -it -v ${PWD}:/app composer install
```
2. run php script to send email
```
MTA_HOST=<mta_host_or_ip>
RECIPIENT_EMAIL_ADDRESS=<recipient_email_address>
docker run --rm -it -w /app -v ${PWD}:/app -e MTA_HOST=$MTA_HOST -e RECIPIENT=$RECIPIENT_EMAIL_ADDRESS php:cli php sendmail.php
```

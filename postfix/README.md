reference
```
https://www.iops.tech/blog/postfix-in-alpine-docker-container/
```

1. copy **.env.dist** to **.env** and set variable 
```
POSTFIX_MYHOSTNAME=local-mta-poc.localhost
POSTFIX_RELAYHOST=<some-ip-or-hostname>
```
2. copy **config/clientACL.cidr.dist** to **config/clientACL.cidr** and edit ip or subnet for allow client to use this postfix mail relay
```
192.168.1.0/24  PERMIT
172.16.0.0/24   PERMIT
0.0.0.0/0       REJECT
```
3. deploy postfix mail relay server
```
docker-compose up -d
```
4. you can update ACL in **config/clientACL.cidr** for add or remove client, if update ACL you have to re-build image
```
docker-compose up -d --build
```